# ex — EXamples or EXperiments

It's a base IntelliJ+Cursive project for personal experimentation and
for sharing examples of problems and solutions with friends and colleagues.

It uses Leiningen instead of Boot for smooth Cursive integration and minimal dependencies.

## Installation

```bash
git clone git@gitlab.com:onetom/ex.git
```

## Examples

### `ex.modern-datomic-test` & `ex.old-datomic-test`

Showcase how to update entities using external keys (UUIDs) instead of
entity IDs (`:db/id`s). It also shows how simple can it be to update entities
together with their referenced entities in one-go.

The modern versions of Datomic (eg. `0.9.5561.50`, released in 2017-06-07)
it is much simpler is it to achieve this vs compared to earlier versions,
eg. `0.9.5350` (from 2016-02-09).

When is this relevant? What's the real-world use-case?

You might want to use external IDs if you expect to expose them to end user
or to other systems which would store it for the long-term. The `:db/id`s however
might change when exporting and importing your data.

<small>TODO: provide reference which explains the reasons for the changing
 `:db/id`s)</small>

Updating references at once is convenient to do when referenced data is presented
 on a UI at once with the ability to edit. In such case we can just let the UI
 modify one composite data structure and send it back to the database as is.
 If we can use the same data structure for displaying and representing updates,
 we can avoid a lot of boring, error prone, specialized code.
 
### REPL startup time test:

```bash
echo '(System/exit 0)' | time env LEIN_FAST_TRAMPOLINE=y lein trampoline repl
```

## License

Copyright © 2017 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
