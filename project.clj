(defproject ex "0.1.0-SNAPSHOT"
  :repositories
  [["my.datomic.com"
    {:url      "https://my.datomic.com/repo",
     :username "<DATOMIC_REPO_USERNAME>",
     :password "<DATOMIC_REPO_PASSWORD>"}]]

  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha17"]
                 [com.datomic/datomic-free "0.9.5561.50"]
                 ;[com.datomic/datomic-pro "0.9.5350" :exclusions [org.slf4j/slf4j-nop org.slf4j/slf4j-log4j12]]
                 [philoskim/debux "0.3.3"]]
  :main ^:skip-aot ex.core
  :jvm-opts ["-XX:+TieredCompilation" "-XX:TieredStopAtLevel=1" "-Xverify:none"]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
