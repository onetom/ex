(ns ex.datomic
  (:require [datomic.api :as d]
            [datomic.config]))

(def datomic-version
  ((nth (->> datomic.config/config-table
             (drop-while (complement (comp (partial = "datomic.version") first)))
             first)
        1)))

(def old-datomic-version "0.9.5350")

(def old-datomic? (= old-datomic-version datomic-version))

(def ^:dynamic *conn*)
(defn tx! [tx] @(d/transact *conn* tx))
(defn db [] (d/db *conn*))
(defn pull [attrs id] (d/pull (db) attrs id))
(defn tid [] (d/tempid :db.part/user))
(defn ent [m] (assoc m :db/id (tid)))

(def attr-tx-fn
  '(let
     [dflt    {:db/ident       attr-name
               :db/valueType   :db.type/string
               :db/cardinality :db.cardinality/one}

      dsl     (fn [opt]
                (let [opt (if (symbol? opt)
                            (keyword opt)
                            opt)]
                  (cond
                    (string? opt)
                    {:db/doc opt}

                    (#{:one 1} opt)
                    {:db/cardinality :db.cardinality/one}

                    (#{:many :m :n} opt)
                    {:db/cardinality :db.cardinality/many}

                    (#{:refs} opt)
                    {:db/valueType   :db.type/ref
                     :db/cardinality :db.cardinality/many}

                    (#{:component :comp} opt)
                    {:db/isComponent true
                     :db/valueType   :db.type/ref}

                    (#{:components :comps} opt)
                    {:db/isComponent true
                     :db/valueType   :db.type/ref
                     :db/cardinality :db.cardinality/many}

                    (#{:unique :uniq} opt)
                    {:db/unique :db.unique/identity}

                    (#{:unique-value :uniq-val} opt)
                    {:db/unique :db.unique/value}

                    (#{:index :idx} opt)
                    {:db/index true}

                    (#{:fulltext} opt)
                    {:db/fulltext true}

                    (#{:no-history} opt)
                    {:db/noHistory true}

                    (#{:keyword
                       :string
                       :boolean
                       :long
                       :bigint
                       :float
                       :double
                       :bigdec
                       :ref
                       :instant
                       :uuid
                       :uri
                       :bytes} opt)
                    {:db/valueType (keyword "db.type" (name opt))})))

      dsl-seq (fn [opt]
                (if (or (vector? opt) (list? opt))
                  (->> opt (map dsl) (apply merge))
                  (dsl opt)))

      enums   (->> opts
                   (filter set?)
                   first
                   (mapv #(hash-map
                            :db/ident (keyword
                                        (str (namespace attr-name)
                                             "." (name attr-name))
                                        (str %)))))]

     (into [(apply merge dflt (map dsl-seq opts))]
           enums)))

(def attr
  (-> {:lang   :clojure
       :params '[_db attr-name & opts]
       :code   attr-tx-fn}
      d/function))

(defn entity-ids
  "Resolve the listed temp IDs into entity IDs
   from the return value of a transaction."
  [{:keys [tempids db-after] :as tx} & tempid-list]
  (map (partial d/resolve-tempid db-after tempids) tempid-list))

(def example-schema
  '[[:attr :str -- "A generic String attr for examples"]
    [:attr :xid -- uniq uuid "An eXternal ID"]
    [:attr :refs -- refs "Generic attr for many-to-many relationships"]])

(defn with-db
  "Create a fixture function which wraps tests into a clean DB
   with the specified or the `ex.datomic/example.schema` schema.

   Usage: (use-fixtures :each (with-db schema))"
  ([] (with-db example-schema))
  ([schema]
   (fn [test-fn]
     (let [db-uri "datomic:mem://example-test"]
       (d/delete-database db-uri)
       (d/create-database db-uri)
       (let [conn (d/connect db-uri)]
         (binding [*conn* conn]
           (tx! [{:db/ident :attr
                  :db/fn    attr
                  :db/id    (d/tempid :db.part/db)}])
           (tx! schema)
           (test-fn)))))))
