(ns ex.datomic.array
  (:require [datomic.api :as d]))

(defn append [db entity-id array-attr item]
  (let [positions (->> entity-id
                       (d/pull db [{array-attr [:pos]}])
                       array-attr
                       (map :pos))
        next-pos  (if (seq positions)
                    (inc (apply max positions))
                    0)]
    [{:db/id     entity-id
      array-attr (assoc item :pos next-pos)}]))


(defn delete [item-id]
  ; It doesn't renumber the remaining item's positions
  ; to maintain continuous numbering because items in operations
  ; are referenced by their (external) IDs
  [[:db.fn/retractEntity item-id]])

(defn- insert [pos item vec]
  (apply conj (subvec vec 0 pos) item (subvec vec pos)))

(defn move [db eid array-attr item-id pos]
  (let [item-dbid (d/entid db item-id)]
    (->> eid
         (d/entity db)
         array-attr
         (sort-by :pos)
         (map :db/id)
         (remove (partial = item-dbid))
         vec
         (insert pos item-dbid)
         (map-indexed #(hash-map :db/id %2 :pos %1))
         vec)))
