(ns ex.datomic.array-test
  (:require [clojure.test :refer :all]
            [debux.core :refer :all]
            [datomic.api :as d]
            [ex.datomic :refer :all]
            [ex.datomic.array :refer :all]))

(def schema
  '[[:attr :xid uniq uuid]
    [:attr :name]
    [:attr :comps comps]
    [:attr :pos long]])

(use-fixtures :each (with-db schema))

(defn- items
  "Returns the component names of the specified entity.
   Component names are sorted by their position.
   The entity is identified by its external ID."
  [xid]
  (->> [:xid xid]
       (d/entity (db))
       :comps
       (sort-by :pos)
       (map :name)))

(defn- positions
  "Returns the sorted component positions of the specified entity.
   The entity is identified by its external ID."
  [xid]
  (->> [:xid xid]
       (d/entity (db))
       :comps
       (map :pos)
       sort))

(deftest append-test
  (let [[x i1 i2] (repeatedly d/squuid)]
    (tx! [{:xid   x
           :name  "Something with ordered components"
           :comps []}])
    (tx! (append (db) [:xid x] :comps {:xid i1 :name "i1"}))
    (tx! (append (db) [:xid x] :comps {:xid i2 :name "i2"}))
    (is (= ["i1" "i2"] (items x)))
    (is (= [0 1] (positions x))
        "Positions are zero based")))

(deftest delete-test
  (let [[x i1 i2 i3 i4] (repeatedly d/squuid)]
    (tx! [{:xid   x}])
    (tx! (append (db) [:xid x] :comps {:xid i1 :name "i1"}))
    (tx! (append (db) [:xid x] :comps {:xid i2 :name "i2"}))
    (tx! (append (db) [:xid x] :comps {:xid i3 :name "i3"}))
    (tx! (append (db) [:xid x] :comps {:xid i4 :name "i4"}))

    (testing "Delete from the middle"
      (tx! (delete [:xid i2]))
      (is (= ["i1" "i3" "i4"] (items x)))
      (is (= [0 2 3] (positions x))
          "Non-continuous numbering is OK"))

    (testing "Delete first item"
      (tx! (delete [:xid i1]))
      (is (= ["i3" "i4"] (items x))))

    (testing "Delete last item"
      (tx! (delete [:xid i4]))
      (is (= ["i3"] (items x))))

    (testing "Deleting already deleted item"
      (tx! (delete [:xid i3]))
      (tx! (delete [:xid i3]))
      (is (= [] (items x))))))

(deftest move-test
  (let [[x i1 i2 i3] (repeatedly d/squuid)]
    (tx! [{:xid   x}])
    (tx! (append (db) [:xid x] :comps {:xid i1 :name "i1"}))
    (tx! (append (db) [:xid x] :comps {:xid i2 :name "i2"}))
    (tx! (append (db) [:xid x] :comps {:xid i3 :name "i3"}))

    (tx! (move (db) [:xid x] :comps [:xid i2] 0))
    (is (= ["i2" "i1" "i3"] (items x)))))
