(ns ex.datomic-test
  (:use debux.core)
  (:require [clojure.test :refer :all]
            [ex.datomic :refer [attr]]))

(defn attr' [name & opts]
  (first (apply attr nil name opts)))

(deftest attr-test
  (testing "default attribute type is string"
    (is (= :db.type/string (:db/valueType (attr' :a))))
    (is (= :db.type/string (:db/valueType (attr' :a []))))
    (is (= :db.type/string (:db/valueType (attr' :a :string))))
    (is (= :db.type/string (:db/valueType (attr' :a 'string))))
    (is (= :db.type/string (:db/valueType (attr' :a '-- 'string))))
    (is (= :db.type/string (:db/valueType (attr' :a [:string])))))

  (testing "default attribute cardinality is one"
    (is (= :db.cardinality/one (:db/cardinality (attr' :a))))
    (is (= :db.cardinality/one (:db/cardinality (attr' :a []))))
    (is (= :db.cardinality/one (:db/cardinality (attr' :a [:one]))))
    (is (= :db.cardinality/one (:db/cardinality (attr' :a [1])))))

  (testing "reference type"
    (is (= :db.type/ref (:db/valueType (attr' :a [:ref])))))

  (testing "component attribute"
    (is (= true (:db/isComponent (attr' :a [:comp])))))

  (testing "unique attribute"
    (is (= :db.unique/identity (:db/unique (attr' :a [:uniq])))))

  (testing "doc string"
    (is (= "attr doc" (:db/doc (attr' :a ["attr doc"])))))

  (testing "doc string can occur at any position in the arg list"
    (is (= "attr doc" (:db/doc (attr' :a [:ref "attr doc"]))))
    (is (= :db.type/ref (:db/valueType (attr' :a [:ref "attr doc"]))))

    (is (= "attr doc" (:db/doc (attr' :a ["attr doc" :ref]))))
    (is (= :db.type/ref (:db/valueType (attr' :a ["attr doc" :ref])))))

  (testing "enums"
    (is (= #{{:db/ident :country.code/US}
             {:db/ident :country.code/HK}}
           (-> (attr nil :country/code :ref #{'US 'HK})
               rest set)))

    (is (= #{{:db/ident :account.payment-method.type/apple-pay}
             {:db/ident :account.payment-method.type/credit-card}}
           (-> (attr nil :account.payment-method/type
                     :ref #{'apple-pay 'credit-card})
               rest set)))))
