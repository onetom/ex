(ns ex.modern-datomic-test
  (:require [clojure.test :refer :all]
            [debux.core :refer :all]
            [datomic.api :as d]
            [ex.datomic :refer :all]))

(assert (not old-datomic?) "These examples only work using modern Datomic")

(use-fixtures :each (with-db))

(deftest transact-test
  (testing "New entity without explicit ID"
    (tx! [{:str "123"}
          {:str "xxx"}])
    (is (= #{{:str "123"}
             {:str "xxx"}}
           (-> '[:find [(pull ?e [:str]) ...]
                 :where [?e :str]]
               (d/q (db)) set)))))

(deftest many-refs-test
  (testing "Update using :db/id"
    (let [{c-eid  "c"
           i1-eid "i1"
           i2-eid "i2"} (-> [{:db/id "c"
                              :str   "Container"
                              :refs  [{:db/id "i1"
                                       :str   "Item 1"}
                                      {:db/id "i2"
                                       :str   "Item 2"}]}]
                            tx! :tempids)
          pull-attrs   '[:db/id :str
                         {:refs [:db/id :str]}]
          pull-c       #(d/pull (db) pull-attrs c-eid)
          c-BEFORE     (pull-c)
          _noop-update (tx! [c-BEFORE])
          c-AFTER      (pull-c)]

      (is (= {:db/id c-eid
              :str   "Container",
              :refs  [{:db/id i1-eid :str "Item 1"}
                      {:db/id i2-eid :str "Item 2"}]}
             c-BEFORE))

      (is (= c-BEFORE
             c-AFTER))))

  (testing "Update using external IDs"
    (let [[c-xid i1-xid i2-xid] (repeatedly d/squuid)
          _            (-> [{:xid  c-xid
                             :str  "Container"
                             :refs [{:xid i1-xid
                                     :str "Item 1"}
                                    {:xid i2-xid
                                     :str "Item 2"}]}]
                           tx!)
          pull-attrs   '[:xid :str
                         {:refs [:xid :str]}]
          pull-c       #(d/pull (db) pull-attrs [:xid c-xid])
          c-BEFORE     (pull-c)
          _noop-update (tx! [c-BEFORE])
          c-AFTER      (pull-c)
          _update      (tx! [(-> c-BEFORE
                                 (assoc-in [:str] "UPDATED Container")
                                 (assoc-in [:refs 0 :str] "UPDATED Item 1"))])
          c-UPDATED    (pull-c)]

      (is (= c-BEFORE c-AFTER))

      (is (= "UPDATED Container"
             (get-in c-UPDATED [:str])))

      (is (= "UPDATED Item 1"
             (get-in c-UPDATED [:refs 0 :str]))))))
