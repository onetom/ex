(ns ex.old-datomic-test
  (:require [clojure.test :refer :all]
            [debux.core :refer :all]
            [datomic.api :as d]
            [ex.datomic :refer :all]))

(use-fixtures :each (with-db))

(deftest transact-test
  (testing "New entity requires explicit ID"
    (tx! [(ent {:str "123"})
          (ent {:str "xxx"})])
    (is (= #{{:str "123"}
             {:str "xxx"}}
           (-> '[:find [(pull ?e [:str]) ...]
                 :where [?e :str]]
               (d/q (db)) set)))
    (when old-datomic?
      (is (thrown-with-msg? Exception #"Missing :db/id"
                            (tx! [{:str "No :db/id"}]))))))

(deftest many-refs-test
  (testing "Update using :db/id"
    (let [[c' i1' i2'] (repeatedly tid)
          [c i1 i2] (-> [{:db/id c'
                          :str   "Container"
                          :refs  [{:db/id i1'
                                   :str   "Item 1"}
                                  {:db/id i2'
                                   :str   "Item 2"}]}]
                        tx!
                        (entity-ids c' i1' i2'))
          pull-attrs   '[:db/id :str
                         {:refs [:db/id :str]}]
          pull-c       #(d/pull (db) pull-attrs c)
          c-BEFORE     (pull-c)
          _noop-update (tx! [c-BEFORE])
          c-AFTER      (pull-c)]

      (is (= {:db/id c
              :str   "Container",
              :refs  [{:db/id i1 :str "Item 1"}
                      {:db/id i2 :str "Item 2"}]}
             c-BEFORE))

      (is (= c-BEFORE c-AFTER))))

  (testing "Update using external IDs"
    (let [[c i1 i2] (repeatedly d/squuid)
          _            (-> [(ent {:xid  c
                                  :str  "Container"
                                  :refs [(ent {:xid i1
                                               :str "Item 1"})
                                         (ent {:xid i2
                                               :str "Item 2"})]})]
                           tx!)
          pull-attrs   '[:xid :str
                         {:refs [:xid :str]}]
          pull-c       #(d/pull (db) pull-attrs [:xid c])
          c-BEFORE     (pull-c)
          xid->eid     (fn [ent]
                         (-> ent
                             (assoc :db/id [:xid (:xid ent)])
                             (dissoc :xid)))
          c-with-EIDs  (-> c-BEFORE
                           (xid->eid)
                           (update-in [:refs 0] xid->eid)
                           (update-in [:refs 1] xid->eid))
          _noop-update (tx! [c-with-EIDs])
          c-AFTER      (pull-c)
          _update      (tx! [(-> c-with-EIDs
                                 (assoc-in [:str] "UPDATED Container")
                                 (assoc-in [:refs 0 :str] "UPDATED Item 1"))])
          c-UPDATED    (pull-c)]

      (is (= {:xid  c
              :str  "Container",
              :refs [{:xid i1, :str "Item 1"}
                     {:xid i2, :str "Item 2"}]}
             c-BEFORE))

      (is (= c-BEFORE c-AFTER))

      (is (= "UPDATED Container"
             (get-in c-UPDATED [:str])))

      (is (= "UPDATED Item 1"
             (get-in c-UPDATED [:refs 0 :str]))))))
